import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.StdStats;

public class PercolationStats {
    private static final double CONFIDENCE_LEVEL_95_CONSTANT = 1.96;
    private final double mean;
    private final double stdDev;
    private final double confidenceLo;
    private final double confidenceHi;

    // perform trials independent experiments on an n-by-n grid
    public PercolationStats(int n, int trials) {
        double[] percolationThresholds = new double[trials];
        for (int i = 0; i < trials; i++) {
            Percolation percolation = new Percolation(n);
            do {
                int randomCell = StdRandom.uniform(n * n);
                int row = (int) Math.floor(((double) randomCell) / n) + 1;
                int column = randomCell % n + 1;
                percolation.open(row, column);//todo troubleshoot boundary indexes .ArrayIndexOutOfBoundsException: 40001
            } while (!percolation.percolates());

            double currentPercolationThreshold = percolation.numberOfOpenSites() / (double) (n * n);
            percolationThresholds[i] = currentPercolationThreshold;
        }

        this.mean = StdStats.mean(percolationThresholds);
        this.stdDev = StdStats.stddev(percolationThresholds);

        double confidenceIntervalDeviation = CONFIDENCE_LEVEL_95_CONSTANT * stdDev / Math.sqrt(trials);
        this.confidenceLo = mean - confidenceIntervalDeviation;
        this.confidenceHi = mean + confidenceIntervalDeviation;
    }

    // sample mean of percolation threshold
    public double mean() {
        return mean;
    }

    // sample standard deviation of percolation threshold
    public double stddev() {
        return stdDev;
    }

    // low  endpoint of 95% confidence interval
    public double confidenceLo() {
        return confidenceLo;
    }

    // high endpoint of 95% confidence interval
    public double confidenceHi() {
        return confidenceHi;
    }

    /**
     * Quick find:
     * Computations took: 0.037894367 seconds for n=20 and 10 trials
     * Computations took: 4.573983937 seconds for n=200 and 10 trials
     * n=2000 took too long
     * Computations took: 50.668434203 seconds for n=400 and 10 trials
     * n=800 took too long
     * Computations took: 135.559846348 seconds for n=500 and 10 trials
     * https://docs.google.com/spreadsheets/d/1Ra2pjjUcDYgoAET1tck0XpHfXA6tsDAW8KKjuaXG8xc/edit?usp=sharing
     * Looks like Percolation algorithm will be cubical with quickFind
     *
     * WeightedUnionFind:
     * Computations took: 0.032397625 seconds for n=20 and 10 trials
     * Computations took: 0.187065441 seconds for n=200 and 10 trials
     * Computations took: 0.360188279 seconds for n=400 and 10 trials
     * Computations took: 0.513877748 seconds for n=500 and 10 trials
     * Computations took: 3.025408405 seconds for n=1000 and 10 trials
     * Computations took: 11.366252488 seconds for n=2000 and 10 trials
     * Computations took: 27.51091583 seconds for n=3000 and 10 trials
     * Conclusion: it shows almost linear growth on such a small inputs. In theory it will be quadratic for large inputs
     */
    public static void main(String[] args) {
        long start = System.nanoTime();
        int n = Integer.parseInt(args[0]);
        int trials = Integer.parseInt(args[1]);
        PercolationStats percolationStats = new PercolationStats(n, trials);
        System.out.println("percolationStats.mean() = " + percolationStats.mean());
        System.out.println("percolationStats.stddev() = " + percolationStats.stddev());
        System.out.printf("95 percent confidence interval = [%f, %f]", percolationStats.confidenceLo(), percolationStats.confidenceHi());
        //todo play with quickUnionFind, weightedUnionFind and meausure time and calculate complexity on different sizes
        long end = System.nanoTime();
        System.out.printf("\nComputations took: %s seconds for n=%s and %s trials", (end - start) / 1E9, n, trials);
    }
}
