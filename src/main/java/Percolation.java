import edu.princeton.cs.algs4.QuickFindUF;
import edu.princeton.cs.algs4.WeightedQuickUnionUF;

public class Percolation {

    private final int firstVirtualCell;
    private final int lastVirtualCell;
//    private final QuickFindUF unionFind;
    private final WeightedQuickUnionUF unionFind;
    private final int n;
    private final boolean[] openState;
    public int openCnt;

    // create n-by-n grid, with all sites blocked
    public Percolation(int n) {
        this.n = n;
        if (this.n < 0) throw new IllegalArgumentException("n<0");
        //adding 2 virtual cells at the top and at the bottom of the n-by-n grid
        int numberOfVirtualElements = 2;
        int numberOfElements = n * n + numberOfVirtualElements;
        openState = new boolean[numberOfElements];
        unionFind = new WeightedQuickUnionUF(numberOfElements);
        firstVirtualCell = 0;
        openState[firstVirtualCell] = true;
        lastVirtualCell = numberOfElements - 1;
        connectLastRowToLastVirtualElement(n);
    }

    private void connectLastRowToLastVirtualElement(int n) {
        for (int i = 1; i <= n; i++) {
            int lastRowCell = lastVirtualCell - i;
            unionFind.union(lastVirtualCell, lastRowCell);//connect last row to last(virtual) element
        }
    }

    // open site (row, col) if it is not open already
    public void open(int row, int col) {
//        System.out.println("Opening " + row + " row and " + col + " column");
        validateIndex(row);
        validateIndex(col);
        int currentCell = getCellIndex(row, col);
        if (!openState[currentCell]) {
            openState[currentCell] = true;
            openCnt++;
        }
        int upCell = getCellIndex(row - 1, col);
        int bottomCell = getCellIndex(row + 1, col);
        int leftCell = getCellIndex(row, col - 1);
        int rightCell = getCellIndex(row, col + 1);
//        System.out.printf("currentCell=%s,upCell=%s,bottomCell=%s,leftCell=%s,rightCell=%s", currentCell, upCell, bottomCell, leftCell, rightCell);
        if (row == 1) {
            unionFind.union(currentCell, firstVirtualCell);
        }
        if (row > 1 && openState[upCell]) {
            unionFind.union(currentCell, upCell);
        }
        if (row > 1 && col > 1 && openState[leftCell]) {
            unionFind.union(currentCell, leftCell);
        }
        if (row > 1 && col < n && openState[rightCell]) {
            unionFind.union(currentCell, rightCell);
        }
        if (row < n && openState[bottomCell]) {
            unionFind.union(currentCell, bottomCell);
        }
    }

    private int getCellIndex(int row, int col) {
        return n * (row - 1) + col + 1;
    }

    private void validateIndex(int index) {
        if (index < 1 || index > n) throw new IllegalArgumentException("outside of the range");
    }

    // is site (row, col) open?
    public boolean isOpen(int row, int col) {
        validateIndex(row);
        validateIndex(col);
        return openState[getCellIndex(row, col)];
    }

    // is site (row, col) full?
    public boolean isFull(int row, int col) {
        validateIndex(row);
        validateIndex(col);
        return unionFind.connected(firstVirtualCell, getCellIndex(row, col));
    }

    // number of open sites
    public int numberOfOpenSites() {
        return openCnt;
    }

    // does the system percolate?
    public boolean percolates() {
        return unionFind.connected(firstVirtualCell, lastVirtualCell);
    }
}
